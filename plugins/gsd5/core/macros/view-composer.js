/*\
title: $:/plugins/gsd5/core/macro/view-composer.js
type: application/javascript
module-type: macro

Macro that creates a view widget based upon the group type Project/Date. Used with the filter-composer macro to group Action by Projects or Dates.


\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

exports.name = "view-composer";

exports.params = [
    {name: "groupTitle"},
    {name: "dateTemplate"}
];

// Run the macro
exports.run = function(groupTitle, dateTemplate) {

    var templateString="",
        linkString='&nbsp;<span style="font-size: 12px; line-height: 18px;"><$link to=<<currentTiddler>>>>></$link></span>';

    // Render any wiki text.
    groupTitle = $tw.wiki.renderText("text/plain","text/vnd.tiddlywiki",groupTitle);
    dateTemplate = $tw.wiki.renderText("text/plain","text/vnd.tiddlywiki",dateTemplate);

    if(groupTitle==="modified"||groupTitle==="created"||groupTitle==="gsd_comp_date") {
        templateString = ' format="date" template="' + dateTemplate + '" ';
        linkString = "";
    }

    return '<$view field="title"' + templateString + ' />' + linkString;
};

})();
