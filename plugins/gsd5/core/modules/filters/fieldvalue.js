/*\
title: $:/plugins/gsd5/core/modules/filters/fieldvalue.js
type: application/javascript
module-type: filteroperator

Filter operator for returning the value of target field.
The filter isn't a true filter and breaks the model, it instead takes a list of tiddlers and extracts values from the specified field.

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

// Export the filter function.
exports._fieldvalue = function(source,operator,options) {
    var results = [],
        fieldname = (operator.operand || "title").toLowerCase();
    if(operator.prefix === "!") {
        source(function(tiddler,title) {
            if(tiddler) {
                var text = tiddler.getFieldString(fieldname);
                if(text !== null) {
                    results.push(text);;
                }
            }
        });
    } else {
        source(function(tiddler,title) {
            if(tiddler) {
                var text = tiddler.getFieldString(fieldname);
                if(text !== null) {
                    results.push(text);
                }
            }
        });
    }
    return results;
};

})();
