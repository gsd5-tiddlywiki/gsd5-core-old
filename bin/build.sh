#!/bin/bash

GSD5_DIR=`pwd`
TIDDLYWIKI5_DIR=$GSD5_DIR/../TiddlyWiki5

OUTPUT_DIR=$GSD5_DIR/../gsd5-output
OUTPUT_FILE=gsd5-empty.html
mkdir -p $OUTPUT_DIR

# Create build date versioning.
BUILD_DATE=`date -u +"%Y%m%d%H%M%S"`
sed -i --regexp-extended "s/(\"version\"\:\W\"[0-9]+\.[0-9]+\.[0-9]+\-*\w*)\+*[0-9]*\"/\1\+$BUILD_DATE\"/" $GSD5_DIR/plugins/gsd5/core/plugin.info

export TIDDLYWIKI_PLUGIN_PATH=$GSD5_DIR/plugins

( cd $TIDDLYWIKI5_DIR && node ./tiddlywiki.js \
	$GSD5_DIR/editions/gsd5 \
	--verbose \
	--output $OUTPUT_DIR \
	--rendertiddler $:/core/save/all $OUTPUT_FILE text/plain )

echo Wrote $OUTPUT_DIR/$OUTPUT_FILE maybe
